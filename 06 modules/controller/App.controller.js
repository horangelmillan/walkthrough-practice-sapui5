sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/m/MessageToast'

], function(Controller, MessageToast) {
    'use strict';
    
    return Controller.extend("sap.ui.practice.walkthrough.controller.App", {
        onShowHello: function () {
            MessageToast.show('Hello world from controller in view');
        }
    });
});