sap.ui.define([
    'sap/m/Button', /* despues de "sap/m/" definimos algo *un elemento* en este caso "Text", RECUERDA: Van con / */

], function (Button) { /* requerimos ese elemento del array (sap.m.<Text>) */
    'use strict';

    new Button({
        text: "Hello World from controls practice"
    }).placeAt("content") /* con placeAt colocamos el texto en el elemento con el id "content" */
});