sap.ui.define([
    "sap/ui/core/mvc/Controller"

], function(Controller) {
    'use strict';

    return Controller.extend("sap.ui.practice.walkthrough.controller.App", {
        onShowHello: function () {
            alert("Hello from controller in a view")
        }
    });
});