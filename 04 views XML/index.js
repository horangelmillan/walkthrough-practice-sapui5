sap.ui.define([
    'sap/ui/core/mvc/XMLView'
    
], function(XMLView) {
    'use strict';
    
    XMLView.create({
        viewName: "sap.ui.practice.walkthrough.view.App"
    }).then(function (oView) {
        oView.placeAt("content");
    });

    XMLView.create({
        viewName: "sap.ui.practice.walkthrough.view.Input"
    }).then(function (oView) {
        oView.placeAt("content");
    });
});