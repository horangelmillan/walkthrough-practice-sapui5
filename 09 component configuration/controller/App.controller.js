sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/m/MessageToast'

], function(Controller, MessageToast) {
    'use strict';
    
    return Controller.extend("sap.ui.practice.walkthrough.controller.App", {
        onShowHello: function () {
            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            var sRecipient = this.getView().getModel().getProperty("/recipient/name");
            var sMsg = oBundle.getText("helloMsg", [sRecipient]);

            MessageToast.show(sMsg);
        },

        onIncrement: function () {
            var textElem = this.getView().byId("num");
            var num = parseInt(textElem.getText());
            if (num > 10) {
                return MessageToast('No more than less')
            }
            var newNum = num + 1;
            textElem.setText(newNum);
        }
    });
});