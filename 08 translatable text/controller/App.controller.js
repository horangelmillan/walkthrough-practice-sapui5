sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/ui/model/json/JSONModel',
    'sap/ui/model/resource/ResourceModel',
    'sap/m/MessageToast'

], function(Controller, JSONModel, ResourceModel, MessageToast) {
    'use strict';
    
    return Controller.extend("sap.ui.practice.walkthrough.controller.App", {
        onInit: function () {
            var oData = {
                recipient: {
                    name: "World",
                    titleButton: "Un boton con otro nombre"
                }
            }

            var oModel = new JSONModel(oData);
            this.getView().setModel(oModel);

            var i18nModel = new ResourceModel({
                bundleName: "sap.ui.practice.walkthrough.i18n.i18n"
            });

            this.getView().setModel(i18nModel, "i18n");
        },

        onShowHello : function () {
            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            var sRecipient = this.getView().getModel().getProperty("/recipient/name");
            var sMsg = oBundle.getText("helloMsg", [sRecipient]);
            // show message
            MessageToast.show(sMsg);
         }
    });
});